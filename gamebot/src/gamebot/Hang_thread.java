package gamebot;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.SecureRandom;
import java.util.Scanner;

import org.json.*;
public class Hang_thread  {
	public String word = new String("http://www.setgetgo.com/randomword/get.php");
	public String answer;
	int id;
	public int flag;
	public String question;
	public Telegrambot tele = new Telegrambot();
	public int count;
	Hang_thread()
	{
		count = 0;
		
	}
	Hang_thread(int cid)
	{
		id = cid;
		flag = 0;
		count = 0;
	}
	public void set_quest()
	{
		int k = answer.length();
		 SecureRandom generator = new SecureRandom();
		 char [] a = answer.toCharArray();
		 for(int i = 0; i< k/2 ; i++)
		 {
			int kl = generator.nextInt(answer.length());
			 if(a[kl] != '_')
				 a[kl] = '_';
			 else
				 i--;
		 }
		 question = new String(a);
		 question.toLowerCase();
		 
	}
	public void run() 
	{
		URL url1 = null;
		try {
			url1 = new URL(word);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//JSONObject word_handle = Telegrambot.get_json(url1);
		InputStreamReader in;
		try {
			in = new InputStreamReader(url1.openStream());
		
		BufferedReader n = new BufferedReader(in);
		String s;
		answer = n.readLine();
		while((s=n.readLine())!= null )
			answer = answer + s;
		answer.toLowerCase();
		}
	 catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		set_quest();
		System.out.println(answer);
		System.out.println(question);
		tele.sendmessage(question,id);
		
	}
	public void cr(char k)
	{
		if(k>=65&&k<=90)
			k+=32;
		else if(k<97)
		{
			tele.sendmessage("enter a valid letter once again",id);
			return;
		}
		int a[] = new int[27];
		for(int i=0;i<26;i++)
			a[i] = -1;
		char b[]=answer.toCharArray();
		char c[] = question.toCharArray();
		for(int i=0;i<b.length;i++)
			if(c[i] == '_')
			{
				
				a[b[i] - 97]++;
			}
		
		if(count<=3 && !answer.equals( question))
		{
			if(a[(k - 97)] == -1)
			{
				count++;
				if(count<=3)
				tele.sendmessage("Wrong guess!Wrong Guesses Left: "+(3-count),id);
			}
			else
			{
				for(int j=0;j<b.length;j++)
					if(b[j] == k)
						c[j] = k;
				a[k - 97] = -1;
			}
			question = new String(c);
			tele.sendmessage(question,id);
		}
			if(answer.equals( question))
			{
				tele.sendmessage("You Won",id);
				
			}
			else if(count > 3 && !(answer.equalsIgnoreCase(question)))
				{
				tele.sendmessage("You Lost",id);
				tele.sendmessage(answer, id);
				}
			if(answer.equals( question) || count > 3 )
			{
			tele.sendmessage("To start a new game type start", id);
			flag = 0;
			count = 0;
			}
		
		
	}
}
